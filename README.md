# Aufgabe: Sortieren mit CompareTo und Comparator

Eine Aufgabe für das PR1-Tutorium (Studiengang Informatik) an der HAW Hamburg.

Autor: Martin Hansen


## Vorbemerkung
Ein anständiges compareTo gibt nur dann 0 zurück, wenn das entsprechende equals ein true zurück geben würde (dies gilt ebenso für  die compare-Methode des Comparator-Interfaces)! Nur in seltenen gut begründeten Ausnahmen darf man davon Ausnahmen machen – hier wollen wir uns Schritt für Schritt an solche Methoden herantasten, deswegen machen wir hier zunächste eine solche Ausnahme.

## Aufgaben:
Im Package *exercise* findet ihr eine Klasse *Gebrauchtangebot.java*, die zum Speichern von Verkaufsangeboten gedacht ist. Außerdem findet ihr dort einen TestFrame, in dem bereits einige Angebote in einer Liste angelegt sind.

1. Macht die Klasse Gebrauchtangebot Comparable! Implementiert das Interface Comparable so, dass man die Liste mit Collections.sort nach Preis aufsteigend sortieren kann. Testet das danach.
2. Implementiert einen Comparator, mit dem man Gebrauchtangebot-Objekte nach Preis absteigend sortieren kann. Testet auch das.
3. Implementiert einen Comparator, der Gebrauchtangebote nach den Namen des Anbieters alphabetisch aufsteigend sortiert. Priorität soll der Nachname haben, bei gleichen Nachnamen soll nach dem Vornamen sortiert werden, bei gleichen Vor- und Nachnamen nach Preis (aufsteigend). Testen nicht vergessen.
4. Jetzt fehlt nur noch ein Comparator, der Gebrauchtangebote zunächst nach nicht verkauft / verkauft (also die nicht verkauften zuerst) sortiert, innerhalb der verkauften / nicht verkauften dann nach den selben Kriterien wie in Aufgabe 4 sortiert. Testet auch diesen.
5. Implementiert Comparatoren, die die Sortierreihenfolgen von 4 und 5 genau umdrehen.
6. Jetzt, wo ihr Übung habt, zurück zum Comparable aus Aufgabe 1:
Spendiert der Gebrauchtangebot-Klasse ein ordentliches equals und überlegt euch ein sinnvolles compareTo, dass nur dann 0 zurück gibt, wenn equals auch true liefert.

Bedenkt: nicht immer muss man alles neu schreiben! Insbesondere braucht ihr nicht das compareTo der Klasse String neu erfinden...