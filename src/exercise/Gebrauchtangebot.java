/*
 * Author:  Martin Hansen
 * Date:    26.11.2012
 * Brief:   Uebungsaufgaben fuer das PR1-Tutorium.
 */

package exercise;

public class Gebrauchtangebot {

    private int preis; // in cent
    private final String beschreibung;
    private final String verkNachname; // Verkaeufer-Nachname
    private final String verkVorname; // Verkaeufer-Vorname
    private boolean verkauft;    

    /**
     * Konstruktor.
     * @param preis der Preis des Angebots in Cent.
     * @param beschreibung der Name des Artikels.
     * @param verkNachname der Nachname des Verkaeufers.
     * @param verkVorname der Vorname des Verkaeufers.
     */
    public Gebrauchtangebot(int preis, String beschreibung, String verkNachname,
            String verkVorname, boolean verkauft) {
        this.preis = preis;
        this.beschreibung = beschreibung;
        this.verkNachname = verkNachname;
        this.verkVorname = verkVorname;
        this.verkauft = verkauft;
    }
    
    public int getPreis() {
        return preis;
    }
    
    public void setPreis(int preis) {
        this.preis = preis;
    }
    
    public boolean isVerkauft() {
        return verkauft;
    }
    
    public void setVerkauft(boolean verkauft) {
        this.verkauft = verkauft;
    }
    
    public String getBeschreibung() {
        return beschreibung;
    }
    
    public String getVerkNachname() {
        return verkNachname;
    }
    
    public String getVerkVorname() {
        return verkVorname;
    }
    
    @Override
    public String toString() {
        return String.format("[<Gebrauchtangebot>: preis=%s; beschreibung=%s; verkNachname=%s; verkVorname=%s; verkauft=%s]",
                        preis, beschreibung, verkNachname, verkVorname, verkauft);
    }

	@Override
	public int hashCode() {
		int result = (beschreibung == null) ? 0 : beschreibung.hashCode();
		result = result + preis;
		result = result + ((verkNachname == null) ? 0 : verkNachname.hashCode());
		result = result + ((verkVorname == null) ? 0 : verkVorname.hashCode());
		result = result + (verkauft ? 3 : 1);
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (this == other){
			return true; // beide Objekte identisch?
		}
		if (other == null){
			return false; // existiert other?
		}
		if (getClass() != other.getClass()){
			return false;// Class-Objekte identisch!
		}

		// Kein Aufruf von super.equals, da es keine (sinnvolle) Super-KLasse gibt.
		
		Gebrauchtangebot otherAngeb = (Gebrauchtangebot)(other);
		return (preis == otherAngeb.preis)
				&& beschreibung.equals(otherAngeb.beschreibung)
				&& verkNachname.equals(otherAngeb.verkNachname)
				&& verkVorname.equals(otherAngeb.verkVorname)
				&& verkauft == otherAngeb.verkauft;

	}
      
    
}
