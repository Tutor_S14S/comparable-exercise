/*
 * Author:  Martin Hansen
 * Date:    08.12.2012
 * Brief:   Uebungsaufgaben fuer das PR1-Tutorium.
 */

package exercise;

import java.util.ArrayList;
import java.util.List;

public class TestFrame_Gebrauchtangebot {

    public static void main(String[] args) {
        
        List<Gebrauchtangebot> angebote = new ArrayList<Gebrauchtangebot>();
        
        angebote.add(new Gebrauchtangebot(50000, "MacBook 5.2", "Hansen", "Martin", false));
        angebote.add(new Gebrauchtangebot(1000, "Apple Remote", "hansen", "martin", false));
        angebote.add(new Gebrauchtangebot(100, "MacBook Pro 15\"", "Hansen", "Martin", true));
        angebote.add(new Gebrauchtangebot(25000, "IPhone 3", "Richter", "Arne", false));
        angebote.add(new Gebrauchtangebot(35000, "iPhone 4", "Richter", "Arne", false));
        angebote.add(new Gebrauchtangebot(80000, "MacBook Air 13\"", "Hans", "Meier", false));
        angebote.add(new Gebrauchtangebot(1200, "Appstore-Gutschein 15 EUR", "Mustermann", "Max", true));
        angebote.add(new Gebrauchtangebot(10000, "MacBook Pro 13\"", "Mustermann", "Meike", false));
        angebote.add(new Gebrauchtangebot(5000, "Installationsanleitung f�r Photoshop", "Fiesling", "Fritz", false));
        angebote.add(new Gebrauchtangebot(5000, "Apple TV", "Hansen", "Kurt", false));
        
        System.out.println("Alle Angebote unsortiert:");
        for(Gebrauchtangebot e: angebote){
            System.out.println(e);
        }
        
        // Jetzt seid ihr dran...
    }

}
